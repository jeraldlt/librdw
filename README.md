# LibRDW

This is a shared system library for redirected walking. It is a lite, unified implementation of several existing RDW algorithms. The hope of the author is that this can be used from a high level by a VR developer in an effective way with minimal prerequisite knowledge about the internal workings of RDW. The current implemented algorithms are:
* No redirection (for testing purposes)
* Constant redirection (for testing purposes)
* Steer to center (S2C)
* NavFunc

## Getting Started
These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. As of now the only supported build platform is Linux, though this project was built with cross-platform compatibility in mind and will hopefully be able to be built on Windows in the near future. These instructions are for a recent Debian based machine, though they should be translatable for any other Linux distro.

### Obtaining the source
There are two ways to obtain the source.

First you can simply download the repository.
```
cd directory/you/want/librdw
wget https://bitbucket.org/jeraldlt/librdw/get/master.zip
unzip master.zip
```

Second you can clone the repository.
If you don't have Git you will need it.
```
sudo apt-get install git
```
```
cd direcectory/you/want/librdw
git clone https://bitbucket.org/jeraldlt/librdw.git
```


### Prerequisites for building on a Linux host

The only prerequisite for building is CMake.

```
sudo apt-get install cmake
```

To build a Windows DLL you will also need mingw-gcc

```
sudo apt-get install mingw-w64
```

### Building on a Linux host

CD to the librdw root directory. For example:
```
cd /home/user/Projects/librdw
```

Make the build directories
```
mkdir -p build/Linux build/Windows
```

To build for Linux: CD to Linux build directory, run CMake, run Make
```
cd build/Linux
cmake ../..
make
```

To build for Windows: CD to the Windows build directory, run CMake with target platform flag, run Make
```
cd build/Windows
cmake -DCMAKE_TARGET_PLATFORM:STRING=WINDOWS ../..
make
```


### Testing on a Linux host
If you are doing active development on the library you do not want to install it every time you rebuild. To overcome this run this command in whichever terminal you will be doing the testing from:
```
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/path/to/librdw/build/Linux
```

## Interfaces
As the project is a system library the source includes interfaces in different languages to utilize the library. The current implemented interfaces are Python (>=2.7) and C# with C++ coming soon. They can be found in the interfaces folder.


## Authors

* **Jerald Thomas** - *Initial work*


## License

This work is currently the private work of Jerald Thomas and he retains all rights to it.



