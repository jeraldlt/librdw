

#include "general.h"
#include "util.h"
#include <math.h>
#include <stdlib.h>
#include <stdio.h>

void none_init(rdw_context_t* context){;}

void constant_init(rdw_context_t* context){;}

void steer_to_center_init(rdw_context_t* context){;}

void steer_to_orbit_init(rdw_context_t* context){;}

void none_gains(rdw_context_t* context, pos_t* user_pos, double* user_theta)
{
    for (int user=0; user<context->num_users; user++) 
    {
        context->translation_gain_x[user] = 1.0;
        context->translation_gain_y[user] = 1.0;
        context->rotation_gain[user] = 1.0;
        context->curvature[user] = 0.0;
    }
}

void constant_gains(rdw_context_t* context, pos_t* user_pos, double* user_theta)
{
    for(int user=0; user<context->num_users; user++) 
    {
        context->translation_gain_x[user] = context->Gt;
        context->translation_gain_y[user] = context->Gt;
        context->rotation_gain[user] = context->Gr;
        context->curvature[user] = context->max_curvature;
    }
}

void steer_to_center_gains(rdw_context_t* context, pos_t* user_pos, double* user_theta)
{
    for(int user=0; user<context->num_users; user++) 
    {
        double delta_pos = distance(context->cur_pos[2 * user], user_pos[user]); // How much user has physically translated since last frame
        double delta_angle = user_theta[user] - context->cur_theta[2 * user]; // How much user has physically rotated since last frame
        
        double new_real_theta = user_theta[user];
        //double new_virtual_theta = context->cur_theta[2 * user + 1];
    
        
        double center[2] = { ((context->ub_rx - context->lb_rx) / 2.0), ((context->ub_ry - context->ub_rx) / 2.0) };
        double x = center[0] - context->cur_pos[2 * user][0];
        double y = center[1] - context->cur_pos[2 * user][1];
        
        double theta_goal = atan2(y, x);
        
        while (theta_goal < 0.0)
            theta_goal += 2 * M_PI;
        while (theta_goal >= 2 * M_PI)
            theta_goal -= 2 * M_PI;
        //printf("theta_goal: %f\n", theta_goal);
        
        int spin = 0;
        
        //if (0);//aprox(theta_goal, 3.14, 0.1))
        //{
        //    spin = 1;
        //}

        double target1 = theta_goal - context->cur_theta[2 * user];
        theta_goal -= 2 * M_PI;
        double target2 = theta_goal - context->cur_theta[2 * user];
        if (fabs(target1) < fabs(target2))
        {
            spin = (int) (target1 / fabs(target1));
        }
        else
        {
            spin = (int) (target2 / fabs(target2));
        }
        
        //spin = 1;
        
        
        //printf("Delta angle: %f; Spin: %d; %f\n", delta_angle, spin, delta_angle*spin);
        
        context->translation_gain_x[user] = 1.0;
        context->translation_gain_y[user] = 1.0;
        if (delta_angle * spin < 0) // This should mean that the user is rotating in the opposite direction that is ideal
        {
            //printf("! ");
            context->rotation_gain[user] = context->gr;// * spin;
            
        }
        else
            context->rotation_gain[user] = context->Gr;// * spin;
        //context->rotation_gain[user] = 1.0 * spin * -1;
        context->curvature[user] = context->max_curvature * spin * -1;
        //context->curvature[user] = 0.0;
        
        //printf("  Spin: %d; Target1: %f; Target2: %f; RotGain: %f; DeltaAngle: %f\n", spin, target1, target2, context->rotation_gain[user], delta_angle);
    
    }
}

void steer_to_orbit_gains(rdw_context_t* context, pos_t* user_pos, double* user_theta)
{
    ;
}
