#include "redirector.h"
#include "general.h"

#include <math.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>

rdw_context_t* rdw_get_context(const unsigned char rdw_alg, const unsigned char num_users)
{
    rdw_context_t* context = (rdw_context_t*) malloc(sizeof(rdw_context_t));
    memset(context, 0, sizeof(rdw_context_t));

    rdw_set_rdw_algorithm(context, rdw_alg);
    context->num_users = num_users;
    context->simulation = 0;

    context->translation_gain = (double*) malloc(sizeof(double) * num_users);
    context->translation_gain_x = (double*) malloc(sizeof(double) * num_users);
    context->translation_gain_y = (double*) malloc(sizeof(double) * num_users);
    context->rotation_gain = (double*) malloc(sizeof(double) * num_users);
    context->curvature = (double*) malloc(sizeof(double) * num_users);
    
    context->cur_pos = (pos_t*) malloc(sizeof(pos_t)*num_users*2);
    memset(context->cur_pos, 0, sizeof(pos_t)*num_users*2);
    context->cur_theta = (double*) malloc(sizeof(double)*num_users*2);
    memset(context->cur_theta, 0, sizeof(double)*num_users*2);
    context->goal_pos = (pos_t*) malloc(sizeof(pos_t)*num_users*2);
    memset(context->goal_pos, 0, sizeof(pos_t)*num_users*2);
    context->goal_theta = (double*) malloc(sizeof(double)*num_users*2);
    memset(context->goal_theta, 0, sizeof(double)*num_users*2);

    return context;
}

void rdw_clear_context(rdw_context_t* context)
{
    free(context->translation_gain);
    context->translation_gain = NULL;
    free(context->translation_gain_x);
    context->translation_gain_x = NULL;
    free(context->translation_gain_y);
    context->translation_gain_y = NULL;
    free(context->rotation_gain);
    context->rotation_gain = NULL;
    free(context->curvature);
    context->curvature = NULL;
    
    free(context->cur_pos);
    context->cur_pos = NULL;
    free(context->cur_theta);
    context->cur_theta = NULL;
    free(context->goal_pos);
    context->goal_pos = NULL;
    free(context->goal_theta);
    context->goal_theta = NULL;

    free(context);
    context = NULL;
}

void rdw_set_rdw_algorithm(rdw_context_t* context, unsigned char rdw_alg)
{
    context->rdw_alg = rdw_alg;

    ////
    // Initialize algorithm specific variables
    ////
    
    switch(rdw_alg)
    {
        case RDW_ALG_NONE:
            none_init(context);
            break;
        
        case RDW_ALG_CONSTANT:
            constant_init(context);
            break;
            
        case RDW_ALG_S2C:
            steer_to_center_init(context);
            break;
            
        case RDW_ALG_S2O:
            steer_to_orbit_init(context);
            break;
            
        default:
            printf("Unknown algorithm\n");
    }
}

void rdw_set_simulation(rdw_context_t* context, unsigned char sim)
{
    context->simulation = sim;
}

void rdw_set_real_range(rdw_context_t* context, double lb_rx, double lb_ry, double ub_rx, double ub_ry)
{
    context->lb_rx = lb_rx;
    context->lb_ry = lb_ry;
    context->ub_rx = ub_rx;
    context->ub_ry = ub_ry;    
}

void rdw_set_virtual_range(rdw_context_t* context, double lb_vx, double lb_vy, double ub_vx, double ub_vy)
{
    context->lb_vx = lb_vx;
    context->lb_vy = lb_vy;
    context->ub_vx = ub_vx;
    context->ub_vy = ub_vy;
}

void rdw_set_translation_gains(rdw_context_t* context, double gt, double Gt)
{
    context->Gt = Gt;
    context->gt = gt;
}

void rdw_set_rotation_gains(rdw_context_t* context, double gr, double Gr)
{
    context->Gr = Gr;
    context->gr = gr;
}

void rdw_set_max_curvature(rdw_context_t* context, double curvature)
{
    context->max_curvature = curvature;
}

void rdw_set_orbit_radius(rdw_context_t* context, double radius)
{
    context->orbit_radius = radius;
}

void rdw_set_user_real_pos(rdw_context_t* context, const unsigned char user, pos_t pos)
{
    for (int i=0; i<2; i++)
        context->cur_pos[2 * user][i] = pos[i];
}

void rdw_set_user_real_theta(rdw_context_t* context, const unsigned char user, double theta)
{
    context->cur_theta[2 * user] = theta;
}

void rdw_set_user_virtual_pos(rdw_context_t* context, const unsigned char user, pos_t pos)
{
    for (int i=0; i<2; i++)
        context->cur_pos[2 * user + 1][i] = pos[i];
}

void rdw_set_user_virtual_theta(rdw_context_t* context, const unsigned char user, double theta)
{
    context->cur_theta[2 * user + 1] = theta;
}

void rdw_set_user_real_pos_goal(rdw_context_t* context, const unsigned char user, rot_t pos)
{
    for (int i=0; i<2; i++)
        context->goal_pos[2 * user][i] = pos[i];
}

void rdw_set_user_real_theta_goal(rdw_context_t* context, const unsigned char user, double theta)
{
    context->goal_theta[2 * user] = theta;
}

void rdw_set_user_virtual_pos_goal(rdw_context_t* context, const unsigned char user, rot_t pos)
{
    for (int i=0; i<2; i++)
        context->goal_pos[2 * user + 1][i] = pos[i];
}

void rdw_set_user_virtual_theta_goal(rdw_context_t* context, const unsigned char user, double theta)
{
    context->goal_theta[2 * user + 1] = theta;
}

unsigned char rdw_get_rdw_algorithm(rdw_context_t* context)
{
    return context->rdw_alg;
}

void rdw_get_user_real_pos(rdw_context_t* context, const unsigned char user, pos_t pos)
{
    pos[0] = context->cur_pos[2 * user][0];
    pos[1] = context->cur_pos[2 * user][1];
}

double rdw_get_user_real_theta(rdw_context_t* context, const unsigned char user)
{
    return context->cur_theta[2 * user];
}

void rdw_get_user_virtual_pos(rdw_context_t* context, const unsigned char user, pos_t pos)
{
    pos[0] = context->cur_pos[2 * user + 1][0];
    pos[1] = context->cur_pos[2 * user + 1][1];
}

double rdw_get_user_virtual_theta(rdw_context_t* context, const unsigned char user)
{
    return context->cur_theta[2 * user + 1];
}

double rdw_get_user_translation_gain(rdw_context_t* context, const unsigned char user)
{
    return context->translation_gain[user];
}

double rdw_get_user_rotation_gain(rdw_context_t* context, const unsigned char user)
{
    return context->rotation_gain[user];
}

double rdw_get_user_curvature(rdw_context_t* context, const unsigned char user)
{
    return context->curvature[user];
}

void rdw_redirect(rdw_context_t* context, pos_t* user_pos, double* user_theta)
{
    
    ////
    // Calculate gains
    ////
    
    switch(context->rdw_alg)
    {
        case RDW_ALG_NONE:
            none_gains(context, user_pos, user_theta);
            break;
        
        case RDW_ALG_CONSTANT:
            constant_gains(context, user_pos, user_theta);
            break;
            
        case RDW_ALG_S2C:
            steer_to_center_gains(context, user_pos, user_theta);
            break;
        
        case RDW_ALG_S2O:
            steer_to_orbit_gains(context, user_pos, user_theta);
            break;
            
        default:
            printf("Unknown algorithm\n");
    }


    ////
    // Apply gains
    ////    
    
    for(int user=0; user<context->num_users; user++) 
    {
        double delta_pos = distance(context->cur_pos[2 * user], user_pos[user]);
        double delta_angle = user_theta[user] - context->cur_theta[2 * user];
        
        double curvature_angle = delta_pos * context->curvature[user];
        double rotation_gain_angle = delta_angle * context->rotation_gain[user];
        
        //printf("%f, %f\n", curvature_angle, rotation_gain_angle);
        //printf("%f, %f, %f\n", context->translation_gain[user], context->rotation_gain[user], context->curvature[user]);
        
        double new_real_theta = user_theta[user];
        double new_virtual_theta = context->cur_theta[2 * user + 1];
        
        // If moving use curvature
        if (delta_pos > 0.01) 
        {
            if (context->simulation == 1)
            {
                new_real_theta += curvature_angle;
            }
            else
            {
                new_virtual_theta -= curvature_angle;
            }
            //printf("curvature: %f\n", context->curvature[user]);
        }
        else
        {
            new_virtual_theta += rotation_gain_angle;
            //printf("rotation_gain: %f\n", context->rotation_gain[user]);
        }

        new_real_theta = clamp_rot(new_real_theta);
        new_virtual_theta = clamp_rot(new_virtual_theta);
        
        // Update user real theta
        rdw_set_user_real_theta(context, user, new_real_theta);
        // Update user virtual theta
        rdw_set_user_virtual_theta(context, user, new_virtual_theta);

        // Update user real pos
        pos_t pos = {0.0};
        
        pos[0] = user_pos[user][0];
        pos[1] = user_pos[user][1];
        rdw_set_user_real_pos(context, user, pos);

        pos[0] = context->cur_pos[2 * user + 1][0] + (delta_pos * context->translation_gain_x[user]) * cos(context->cur_theta[2 * user + 1]);
        pos[1] = context->cur_pos[2 * user + 1][1] + (delta_pos * context->translation_gain_y[user]) * sin(context->cur_theta[2 * user + 1]);
        rdw_set_user_virtual_pos(context, user, pos);


        user_pos[user][0] = pos[0];
        user_pos[user][1] = pos[1];
        user_theta[user] = new_virtual_theta;
    }
}
