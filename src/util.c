#include "util.h"
#include <math.h>
#include <stdlib.h>

double distance(pos_t x1, pos_t x2)
{
    double sum = 0.0;
    for (int i=0; i<2; i++)
    {
        sum += pow((x2[i] - x1[i]), 2);
    }

    return sqrt(sum);
}

void normalize_vec2(vec2_t x)
{
    double sum = 0.0;

    for (int i=0; i<2; i++)
    {
        sum += pow(x[i], 2);
    }

    double length = sqrt(sum);

    for (int i=0; i<2; i++)
    {
        x[i] /= length;
    }
}

double angle(double x, double y)
{
    return atan2(y, x);
}

double clamp(double val, double min, double max)
{
    if (val <= min)
        return min;
    if (val >= max)
        return max;
    return val;
}

int aprox(double val, double target, double epsilon)
{
    if(fabs(target - val) < epsilon)
        return 1;
    return 0;
}

double clamp_rot(double rot)
{
    while (rot > 2 * M_PI)
    {
        rot -= 2 * M_PI;
    }
    while (rot <= 0)
    {
        rot += 2 * M_PI;
    }
    
    return rot;
}

int quadrant(double x, double y)
{
    int quad = 0;
    if (x >= 0.0)
    {
        if (y > 0.0)
            quad = 1;
        else
            quad = 2;
    }
    else
    {
        if (y > 0.0)
            quad = 4;
        else
            quad = 3;
    }
    
    return quad;
}

int sign(double val)
{
    if (aprox(val, 0.0, 0.0001) == 1)
        return 1;
    
    return (int) (val / fabs(val));
}
