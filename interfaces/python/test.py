from librdw import *
from math import pi

def pretty_list(l):
    s = "["
    for i in l:
        s += "%.5f " %i
    s = s[:-1] + ']'
    return s

rdw = Redirector(0, 1)
rdw.set_translation_gains(0.5, 2.0)
rdw.set_rotation_gains(0.5, 2.0)
rdw.set_max_curvature(1.0/22.0)

rdw.set_user_real_pos(0, [0.0, 0.0])
rdw.set_user_real_theta(0, 0.0)
rdw.set_user_virtual_pos(0, [0.0, 0.0])
rdw.set_user_virtual_theta(0, 0.0)

pos = [0.0, 0.0]
theta = 0.0

for i in range(5):
    pos[0] += 0.1
    rdw.redirect([pos], [theta])
    print "%i:"%i, pretty_list(rdw.get_user_real_pos(0)), rdw.get_user_real_theta(0), pretty_list(rdw.get_user_virtual_pos(0)), rdw.get_user_virtual_theta(0)
 
theta = pi/2.0
   
for i in range(5, 10):
    pos[0] += 0.1
    rdw.redirect([pos], [theta])
    print "%i:"%i, pretty_list(rdw.get_user_real_pos(0)), rdw.get_user_real_theta(0), pretty_list(rdw.get_user_virtual_pos(0)), rdw.get_user_virtual_theta(0)
