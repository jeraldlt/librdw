import platform
lib = None
if platform.system() == "Linux":
    lib = "librdw.so"
elif platform.system() == "Windows":
    lib = "librdw.DLL"
else:
    print "Unsupported platform"
    import sys
    sys.exit(1)

from ctypes import *

RDW_ALG_NONE     = 0
RDW_ALG_CONSTANT = 1
RDW_ALG_S2C      = 2
RDW_ALG_S2O      = 3
RDW_ALG_NAVFUNC  = 4
RDW_ALG_SYSTEM   = 5

class Redirector(object):
    so = CDLL(lib)
    so.rdw_get_context.restype = c_long
    so.rdw_get_user_real_theta.restype = c_double
    so.rdw_get_user_virtual_theta.restype = c_double
    so.rdw_get_user_translation_gain.restype = c_double
    so.rdw_get_user_rotation_gain.restype = c_double
    so.rdw_get_user_curvature.restype = c_double
    
    
    def __init__(self, alg, num_users):
        self.context = c_long(Redirector.so.rdw_get_context(alg, num_users))
        
    def __del__(self):
        Redirector.so.rdw_clear_context(self.context)
        
    def set_simulation(self, sim):
        if sim:
            Redirector.so.rdw_set_simulation(self.context, 1)
        else:
            Redirector.so.rdw_set_simulation(self.context, 0)

    def set_real_range(self, min_x, min_y, max_x, max_y):
        Redirector.so.rdw_set_real_range(self.context, c_double(min_x), c_double(min_y), c_double(max_x), c_double(max_y))
        
    def set_virtual_range(self, min_x, min_y, max_x, max_y):
        Redirector.so.rdw_set_virtual_range(self.context, c_double(min_x), c_double(min_y), c_double(max_x), c_double(max_y))
    
    def set_translation_gains(self, gt, Gt):
        Redirector.so.rdw_set_translation_gains(self.context, c_double(gt), c_double(Gt))
        
    def set_rotation_gains(self, gr, Gr):
        Redirector.so.rdw_set_rotation_gains(self.context, c_double(gr), c_double(Gr))
        
    def set_max_curvature(self, curvature):
        Redirector.so.rdw_set_max_curvature(self.context, c_double(curvature))
        
    def set_user_real_pos(self, user, pos):
        Redirector.so.rdw_set_user_real_pos(self.context, user, (c_double * 2)(*pos))
    
    def set_user_real_theta(self, user, theta):
        Redirector.so.rdw_set_user_real_theta(self.context, user, c_double(theta))
    
    def set_user_virtual_pos(self, user, pos):
        Redirector.so.rdw_set_user_virtual_pos(self.context, user, (c_double * 2)(*pos))
    
    def set_user_virtual_theta(self, user, theta):
        Redirector.so.rdw_set_user_virtual_theta(self.context, user, c_double(theta))
        
    def set_user_real_pos_goal(self, user, pos):
        Redirector.so.rdw_set_user_real_pos_goal(self.context, user, (c_double * 2)(*pos))
        
    def set_user_real_theta_goal(self, user, theta):
        Redirector.so.rdw_set_user_real_theta_goal(self.context, user, c_double(theta))
        
    def set_user_virtual_pos_goal(self, user, pos):
        Redirector.so.rdw_set_user_virtual_pos_goal(self.context, user, (c_double * 2)(*pos))
        
    def set_user_virtual_theta_goal(self, user, theta):
        Redirector.so.rdw_set_user_virtual_theta_goal(self.context, user, c_double(theta))
        
    def set_epsilon(self, epsilon):
        Redirector.so.rdw_nav_set_epsilon(self.context, c_double(epsilon))
        
    def set_mu(self, mu):
        Redirector.so.rdw_nav_set_mu(self.context, c_double(mu))
        
    def set_step(self, step):
        Redirector.so.rdw_nav_set_step(self.context, c_double(step))

    def set_user_translation_gain(self, user, gain):
        Redirector.so.rdw_set_user_translation_gain(self.context, user, c_double(gain))

    def set_user_rotation_gain(self, user, gain):
        Redirector.so.rdw_set_user_rotation_gain(self.context, user, c_double(gain))

    def set_user_curvature(self, user, gain):
        Redirector.so.rdw_set_user_curvature(self.context, user, c_double(gain))

    def get_user_real_pos(self, user):
        arr = (c_double * 2)()
        Redirector.so.rdw_get_user_real_pos(self.context, user, arr)
        return list(arr)
    
    def get_user_real_theta(self, user):
        return Redirector.so.rdw_get_user_real_theta(self.context, user)
        
    def get_user_virtual_pos(self, user):
        arr = (c_double * 2)()
        Redirector.so.rdw_get_user_virtual_pos(self.context, user, arr)
        return list(arr)
    
    def get_user_virtual_theta(self, user):
        return Redirector.so.rdw_get_user_virtual_theta(self.context, user)

    def get_user_translation_gain(self, user):
        return Redirector.so.rdw_get_user_translation_gain(self.context, user)
        
    def get_user_rotation_gain(self, user):
        return Redirector.so.rdw_get_user_rotation_gain(self.context, user)
        
    def get_user_curvature(self, user):
        return Redirector.so.rdw_get_user_curvature(self.context, user)
        
    
    def redirect(self, user_pos, user_theta):
        
        pos_arr = ((c_double * 2) * len(user_pos))()
        for i in range(len(user_pos)):
            pos_arr[i][0] = user_pos[i][0]
            pos_arr[i][1] = user_pos[i][1]
        
        theta_arr = (c_double * len(user_theta))()
        for i in range(len(user_theta)):
            theta_arr[i] = user_theta[i]
        
        Redirector.so.rdw_redirect(self.context, pos_arr, theta_arr)
        
        return [[x[0], x[1]] for x in pos_arr], [x for x in theta_arr]
        
