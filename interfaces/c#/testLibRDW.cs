using System;
using LibRDW;

public class testLibRDW
{
    static public void Main()
    {
        Redirector rdw = new Redirector(RDWAlgorithm.NAVFUNC, 1);
        rdw.SetRealRange(0.0, 0.0, 10.0, 10.0);
        rdw.SetVirtualRange(0.0, 0.0, 20.0, 20.0);
        
        rdw.SetEpsilon(0.001);
        rdw.SetMu(3.0);
        rdw.SetStep(0.05);
        
        rdw.SetTranslationGains(0.5, 2.0);
        rdw.SetRotationGains(0.5, 2.0);
        rdw.SetMaxCurvature(1.0/22.0);

        rdw.SetUserRealPos(0, new double[] { 0.0, 0.0 });
        rdw.SetUserRealTheta(0, 0.0);
        rdw.SetUserVirtualPos(0, new double[] { 0.0, 0.0 });
        rdw.SetUserVirtualTheta(0, Math.PI/2.0);
        
        rdw.SetUserRealPosGoal(0, new double[] { 5.0, 5.0 });
        rdw.SetUserRealThetaGoal(0, 0.0);
        rdw.SetUserVirtualPosGoal(0, new double[] { 10.0, 10.0 });
        rdw.SetUserVirtualThetaGoal(0, Math.PI/2.0);
        
        double[,] pos = new double[,] { { 0.0, 0.0 } };
        double[] theta = { 0.0 };
        
        rdw.Redirect(pos, theta);
        
        Console.WriteLine(rdw.GetUserTranslationGain(0));
        Console.WriteLine(rdw.GetUserRotationGain(0));
        Console.WriteLine(rdw.GetUserCurvature(0));
        
        
    }
}
