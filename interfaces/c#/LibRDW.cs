﻿using System;
using System.Runtime.InteropServices;


namespace LibRDW
{
    
    public enum RDWAlgorithm { NONE, CONSTANT, S2C, S2O, NAVFUNC };

    public class Redirector
    {
        private IntPtr context;
        private const string lib = "librdw";
        
        
        [DllImport(lib, CallingConvention=CallingConvention.StdCall)]
        private static extern IntPtr rdw_get_context(ushort alg, ushort num_users);

        [DllImport(lib, CallingConvention=CallingConvention.StdCall)]
        private static extern void rdw_clear_context(IntPtr context);

        [DllImport(lib, CallingConvention = CallingConvention.StdCall)]
        private static extern void rdw_set_real_range(IntPtr context, double lb_rx, double lb_ry, double ub_rx, double ub_ry);

        [DllImport(lib, CallingConvention = CallingConvention.StdCall)]
        private static extern void rdw_set_virtual_range(IntPtr context, double lb_vx, double lb_vy, double ub_vx, double ub_vy);

        [DllImport(lib, CallingConvention=CallingConvention.StdCall)]
        private static extern void rdw_set_translation_gains(IntPtr context, double gt, double Gt);

        [DllImport(lib, CallingConvention=CallingConvention.StdCall)]
        private static extern void rdw_set_rotation_gains(IntPtr context, double gr, double Gr);

        [DllImport(lib, CallingConvention=CallingConvention.StdCall)]
        private static extern void rdw_set_max_curvature(IntPtr context, double radius);

        [DllImport(lib, CallingConvention=CallingConvention.StdCall)]
        private static extern void rdw_set_user_real_pos(IntPtr context, ushort user, double[] pos);

        [DllImport(lib, CallingConvention = CallingConvention.StdCall)]
        private static extern void rdw_set_user_real_pos_goal(IntPtr context, ushort user, double[] pos);

        [DllImport(lib, CallingConvention=CallingConvention.StdCall)]
        private static extern void rdw_set_user_real_theta(IntPtr context, ushort user, double theta);

        [DllImport(lib, CallingConvention = CallingConvention.StdCall)]
        private static extern void rdw_set_user_real_theta_goal(IntPtr context, ushort user, double theta);

        [DllImport(lib, CallingConvention=CallingConvention.StdCall)]
        private static extern void rdw_set_user_virtual_pos(IntPtr context, ushort user, double[] pos);

        [DllImport(lib, CallingConvention = CallingConvention.StdCall)]
        private static extern void rdw_set_user_virtual_pos_goal(IntPtr context, ushort user, double[] pos);

        [DllImport(lib, CallingConvention=CallingConvention.StdCall)]
        private static extern void rdw_set_user_virtual_theta(IntPtr context, ushort user, double theta);

        [DllImport(lib, CallingConvention = CallingConvention.StdCall)]
        private static extern void rdw_set_user_virtual_theta_goal(IntPtr context, ushort user, double theta);

        [DllImport(lib, CallingConvention = CallingConvention.StdCall)]
        private static extern char rdw_get_rdw_algorithm(IntPtr context);

        [DllImport(lib, CallingConvention=CallingConvention.StdCall)]
        private static extern void rdw_get_user_real_pos(IntPtr context, ushort user, double[] pos);

        [DllImport(lib, CallingConvention=CallingConvention.StdCall)]
        private static extern double rdw_get_user_real_theta(IntPtr context, ushort user);

        [DllImport(lib, CallingConvention=CallingConvention.StdCall)]
        private static extern void rdw_get_user_virtual_pos(IntPtr context, ushort user, double[] pos);

        [DllImport(lib, CallingConvention=CallingConvention.StdCall)]
        private static extern double rdw_get_user_virtual_theta(IntPtr context, ushort user);
        
        [DllImport(lib, CallingConvention=CallingConvention.StdCall)]
        private static extern double rdw_get_user_translation_gain(IntPtr context, ushort user);
        
        [DllImport(lib, CallingConvention=CallingConvention.StdCall)]
        private static extern double rdw_get_user_rotation_gain(IntPtr context, ushort user);
        
        [DllImport(lib, CallingConvention=CallingConvention.StdCall)]
        private static extern double rdw_get_user_curvature(IntPtr context, ushort user);

        [DllImport(lib, CallingConvention=CallingConvention.StdCall)]
        private static extern void rdw_redirect(IntPtr context, double[,] pos, double[] theta);
        
        [DllImport(lib, CallingConvention=CallingConvention.StdCall)]
        private static extern void rdw_nav_set_epsilon(IntPtr context, double epsilon);
        
        [DllImport(lib, CallingConvention=CallingConvention.StdCall)]
        private static extern void rdw_nav_set_mu(IntPtr context, double mu);
        
        [DllImport(lib, CallingConvention=CallingConvention.StdCall)]
        private static extern void rdw_nav_set_step(IntPtr context, double step);


        public Redirector(RDWAlgorithm alg, int num_users)
        {
            context = rdw_get_context((ushort)alg, (ushort)num_users);

        }

        ~Redirector()
        {
            rdw_clear_context(context);
        }

        public void SetRealRange(double lb_rx, double lb_ry, double ub_rx, double ub_ry)
        {
            rdw_set_real_range(context, lb_rx, lb_ry, ub_rx, ub_ry);
        }

        public void SetVirtualRange(double lb_vx, double lb_vy, double ub_vx, double ub_vy)
        {
            rdw_set_virtual_range(context, lb_vx, lb_vy, ub_vx, ub_vy);
        }

        public void SetTranslationGains(double gt, double Gt)
        {
            rdw_set_translation_gains(context, gt, Gt);
        }

        public void SetRotationGains(double gr, double Gr)
        {
            rdw_set_rotation_gains(context, gr, Gr);
        }

        public void SetMaxCurvature(double curvature)
        {
            rdw_set_max_curvature(context, curvature);
        }

        public void SetUserRealPos(ushort user, double[] pos)
        {
            rdw_set_user_real_pos(context, user, pos);
        }

        public void SetUserRealPosGoal(ushort user, double[] pos)
        {
            rdw_set_user_real_pos_goal(context, user, pos);
        }

        public void SetUserRealTheta(ushort user, double theta)
        {
            rdw_set_user_real_theta(context, user, theta);
        }

        public void SetUserRealThetaGoal(ushort user, double theta)
        {
            rdw_set_user_real_theta_goal(context, user, theta);
        }

        public void SetUserVirtualPos(ushort user, double[] pos)
        {
            rdw_set_user_virtual_pos(context, user, pos);
        }

        public void SetUserVirtualPosGoal(ushort user, double[] pos)
        {
            rdw_set_user_virtual_pos_goal(context, user, pos);
        }

        public void SetUserVirtualTheta(ushort user, double theta)
        {
            rdw_set_user_virtual_theta(context, user, theta);
        }

        public void SetUserVirtualThetaGoal(ushort user, double theta)
        {
            rdw_set_user_virtual_theta_goal(context, user, theta);
        }

        public RDWAlgorithm GetRDWAlgorithm()
        {
            return (RDWAlgorithm)rdw_get_rdw_algorithm(context);
        }

        public double[] GetUserRealPos(ushort user)
        {
            double[] pos = { 0.0, 0.0 };
            rdw_get_user_real_pos(context, user, pos);
            return pos;
        }

        public double GetUserRealTheta(ushort user)
        {
            return rdw_get_user_real_theta(context, user);
        }

        public double[] GetUserVirtualPos(ushort user)
        {
            double[] pos = { 0.0, 0.0 };
            rdw_get_user_virtual_pos(context, user, pos);
            return pos;
        }

        public double GetUserVirtalTheta(ushort user)
        {
            return rdw_get_user_virtual_theta(context, user);
        }
        
        public double GetUserTranslationGain(ushort user)
        {
            return rdw_get_user_translation_gain(context, user);
        }
        
        public double GetUserRotationGain(ushort user)
        {
            return rdw_get_user_rotation_gain(context, user);
        }
        
        public double GetUserCurvature(ushort user)
        {
            return rdw_get_user_curvature(context, user);
        }
        
        public void SetEpsilon(double epsilon)
        {
            rdw_nav_set_epsilon(context, epsilon);
        }
        
        public void SetMu(double mu)
        {
            rdw_nav_set_mu(context, mu);
        }
        
        public void SetStep(double step)
        {
            rdw_nav_set_step(context, step);
        }

        public void Redirect(double[,] pos, double[] theta)
        {
            rdw_redirect(context, pos, theta);
        }
    }

}
