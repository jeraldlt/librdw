# Create target platform cache variable
set(CMAKE_TARGET_PLATFORM "LINUX" CACHE STRING "Build target (LINUX | WINDOWS)")
# default to hidden symbols
set(CMAKE_C_VISIBILITY_PRESET hidden)
# set stadard to C11
set(CMAKE_C_STANDARD 11)
set(CMAKE_CXX_STANDARD 11)

if (CMAKE_TARGET_PLATFORM STREQUAL "LINUX")
    message("Building for Linux...")
endif()

# If cross compiling to windows
# Requires MinGW32-gcc
# sudo apt-get install mingw-w64
# add -DCMAKE_TARGET_PLATFORM:STRING=WINDOWS to cmake command
if (CMAKE_TARGET_PLATFORM STREQUAL "WINDOWS")
    message("Building for Windows...")
    set(CMAKE_C_COMPILER /usr/bin/x86_64-w64-mingw32-gcc)
    SET(CMAKE_SHARED_LIBRARY_LINK_C_FLAGS) 
endif()

add_library(rdw SHARED ${SOURCE_FILES})

if (CMAKE_TARGET_PLATFORM STREQUAL "WINDOWS")
    set_target_properties(rdw PROPERTIES SUFFIX ".DLL")
endif()

if (CMAKE_TARGET_PLATFORM STREQUAL "LINUX")
    target_link_libraries(rdw m)
endif()
