#ifndef GENERAL_H
#define GENERAL_H

#include "types.h"

void none_init(rdw_context_t* context);

void constant_init(rdw_context_t* context);

void steer_to_center_init(rdw_context_t* context);

void steer_to_orbit_init(rdw_context_t* context);

void none_gains(rdw_context_t* context, pos_t* user_pos, double* user_theta);

void constant_gains(rdw_context_t* context, pos_t* user_pos, double* user_theta);

void steer_to_center_gains(rdw_context_t* context, pos_t* user_pos, double* user_theta);

void steer_to_orbit_gains(rdw_context_t* context, pos_t* user_pos, double* user_theta);

#endif // GENERAL_H
