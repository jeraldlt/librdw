#ifndef CONTEXT_H
#define CONTEXT_H

typedef double vec2_t[2];
typedef double pos_t[2];
typedef double rot_t[2];

#ifdef BUILD_WITH_NAVFUNC
typedef double config_t[6];
typedef double H_t[12][6];
typedef double k_t[12];
#endif // BUILD_WITH_NAVFUNC

typedef struct rdw_context_t
{
    unsigned char rdw_alg;   // Redirection algorithm to use
    unsigned char num_users;  // Number of users in system

    unsigned char simulation;
    
    double ub_rx;  // X upper bound of real environment
    double ub_ry;  // Y upper bound of real environment
    double ub_vx;  // X upper bound of virtual environment
    double ub_vy;  // Y upper bound of virtual environment
    
    double lb_rx;  // X lower bound of real environment
    double lb_ry;  // Y lower bound of real environment
    double lb_vx;  // X lower bound of virtual environment
    double lb_vy;  // Y lower bound of virtual environment
    
    double Gt;  // Upper bound of translation gain
    double gt;  // Lower bound of translation gain
    double Gr;  // Upper bound of rotation gain
    double gr;  // Lower bound of rotation gain

    double max_curvature;     // maximum curvature (1/r)
    double orbit_radius;      // target radius for S2O
    
    double* translation_gain;
    double* translation_gain_x;
    double* translation_gain_y;
    double* rotation_gain;
    double* curvature;
    
    pos_t* cur_pos;
    double* cur_theta;
    pos_t* goal_pos;
    double* goal_theta;

    double ms;  // Total milliseconds since last call to redirect()
}rdw_context_t;


#endif // CONTEXT_H
