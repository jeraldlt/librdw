#ifndef REDIRECTOR_H
#define REDIRECTOR_H

#if defined _WIN32 || __CYGWIN__
    #ifdef __GNUC__
        #define API __attribute__ ((dllexport))
    #else
        #define API __declspec(dllexport)
    #endif
#else
    #define API __attribute__ ((visibility ("default")))
#endif

#include "types.h"
#include <time.h>


/*
 Creates the rdw_context_t struct
     Arguments:
         rdw_alg -> redirection algorithm
     Returns:
         pointer to an initialized rdw_context_t struct
*/
API rdw_context_t* rdw_get_context(const unsigned char rdw_alg, const unsigned char num_users);

/*
 Clears the rdw_context_t struct and frees memory
     Arguments:
         context -> rdw_context_t pointer to be cleared
*/
API void rdw_clear_context(rdw_context_t* context);

/*
 Sets the RDW algorithm for the context
     Arguments:
         context -> rdw_context_t pointer to be cleared
         rdw_alg -> redirection algorithm
*/
API void rdw_set_rdw_algorithm(rdw_context_t* context, const unsigned char rdw_alg);

API void rdw_set_simulation(rdw_context_t* context, unsigned char sim);

API void rdw_set_real_range(rdw_context_t* context, double lb_rx, double lb_ry, double ub_rx, double ub_ry);

API void rdw_set_virtual_range(rdw_context_t* context, double lb_vx, double lb_vy, double ub_vx, double ub_vy);

API void rdw_set_translation_gains(rdw_context_t* context, double gt, double Gt);

API void rdw_set_rotation_gains(rdw_context_t* context, double gr, double Gr);

API void rdw_set_max_curvature(rdw_context_t* context, double curvature);

API void rdw_set_orbit_radius(rdw_context_t* context, double radius);

API void rdw_set_user_real_pos(rdw_context_t* context, const unsigned char user, pos_t pos);

API void rdw_set_user_real_theta(rdw_context_t* context, const unsigned char user, double theta);

API void rdw_set_user_virtual_pos(rdw_context_t* context, const unsigned char user, pos_t pos);

API void rdw_set_user_virtual_theta(rdw_context_t* context, const unsigned char user, double theta);

API void rdw_set_user_real_pos_goal(rdw_context_t* context, const unsigned char user, rot_t pos);

API void rdw_set_user_real_theta_goal(rdw_context_t* context, const unsigned char user, double theta);

API void rdw_set_user_virtual_pos_goal(rdw_context_t* context, const unsigned char user, rot_t pos);

API void rdw_set_user_virtual_theta_goal(rdw_context_t* context, const unsigned char user, double theta);

API unsigned char rdw_get_rdw_algorithm(rdw_context_t* context);

API void rdw_get_user_real_pos(rdw_context_t* context, const unsigned char user, pos_t pos);

API double rdw_get_user_real_theta(rdw_context_t* context, const unsigned char user);

API void rdw_get_user_virtual_pos(rdw_context_t* context, const unsigned char user, pos_t pos);

API double rdw_get_user_virtual_theta(rdw_context_t* context, const unsigned char user);

API double rdw_get_user_translation_gain(rdw_context_t* context, const unsigned char user);

API double rdw_get_user_rotation_gain(rdw_context_t* context, const unsigned char user);

API double rdw_get_user_curvature(rdw_context_t* context, const unsigned char user);

API void rdw_redirect(rdw_context_t* context, pos_t* user_pos, double* user_theta);


#define RDW_ALG_NONE     0  // Will apply no redirection
#define RDW_ALG_CONSTANT 1  // Will apply a constant redirection using Gt, Gr, and max_curvature
#define RDW_ALG_S2C      2  // Steer to center as defined by Sharif Razzaque
#define RDW_ALG_S2O      3  // Steer to orbit as defined by Sharif Razzaque

#endif // REDIRECTOR_H
