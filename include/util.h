#ifndef UTIL_H
#define UTIL_H

#include "types.h"

double distance(pos_t x1, pos_t x2);

void normalize_vec2(vec2_t x);

double angle(double x, double y);

double clamp(double val, double min, double max);

int aprox(double val, double target, double epsilon);

double clamp_rot(double rot);

int quadrant(double x, double y);

int sign(double val);

#endif // UTIL_H
